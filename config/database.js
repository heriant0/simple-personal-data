module.exports = {
  "development": {
    "username": "postgres",
    "password": "postgres",
    "database": "db_personal_data",
    "host": "127.0.0.1",
    "dialect": "postgres",
    "logging": false
  },
  "test": {
    "username": "root",
    "password": null,
    "database": "database_test",
    "host": "127.0.0.1",
    "dialect": "mysql"
  },
  "production": {
    "username": "pqgckzxdoodzjg",
    "password": "917c24d5ac291596b0f3429818d08cfbcd0bc99ef5d261d8207c9d6b4dc78c7c",
    "database": "simple-personal-data",
    "host": "ec2-44-198-100-81.compute-1.amazonaws.com",
    "dialect": "postgres",
    "dialectOptions": {
      "ssl": { rejectUnauthorized: false }
    }
  }
}