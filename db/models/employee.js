'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class employee extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      employee.belongsTo(models.master_religion, {
        foreignKey: "religion_id"
      })
      employee.belongsTo(models.master_marital_status, {
        foreignKey: "marital_status_id"
      })
    }
  };
  employee.init({
    employee_code: DataTypes.STRING,
    name: DataTypes.STRING,
    email: {
      type: DataTypes.STRING,
      validate: {
        isEmail: {
          arg: true,
          msg: "Invalid format email"
        }
      }
    },
    phone_number: DataTypes.STRING,
    place_of_birth: DataTypes.STRING,
    date_of_birth: DataTypes.DATE,
    gender: DataTypes.INTEGER,
    religion_id: DataTypes.INTEGER,
    blood_type: DataTypes.STRING,
    marital_status_id: DataTypes.INTEGER,
    citizenship: DataTypes.STRING,
    identity_number: DataTypes.STRING,
    npwp_number: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'employee',
  });
  return employee;
};