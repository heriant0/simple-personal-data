'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class education extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      education.belongsTo(models.employee, {
        foreignKey: "employee_id"
      })
      education.belongsTo(models.master_education_level, {
        foreignKey: "education_level_id"
      })
    }
  };
  education.init({
    employee_id: DataTypes.INTEGER,
    education_level_id: DataTypes.INTEGER,
    start_year: DataTypes.INTEGER,
    graduation_date: DataTypes.DATE,
    major: DataTypes.STRING,
    gpa: DataTypes.DOUBLE
  }, {
    sequelize,
    modelName: 'education',
  });
  return education;
};