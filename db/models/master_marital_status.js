'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class master_marital_status extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  master_marital_status.init({
    name: DataTypes.STRING
  }, {
    sequelize,
    modelName: 'master_marital_status',
  });
  return master_marital_status;
};