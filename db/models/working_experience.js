'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class working_experience extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
    }
  };
  working_experience.init({
    employee_id: DataTypes.INTEGER,
    company_name: DataTypes.STRING,
    business_industry: DataTypes.STRING,
    last_position: DataTypes.STRING,
    job_description: DataTypes.STRING,
    join_date: DataTypes.DATE,
    resignation_date: DataTypes.DATE
  }, {
    sequelize,
    modelName: 'working_experience',
  });
  return working_experience;
};