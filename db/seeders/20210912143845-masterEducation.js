'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
    await queryInterface.bulkInsert('master_education_levels', [{
      code: 'SD',
      name: 'Sekolah Dasar',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      code: 'SMP',
      name: 'Sekolah Menengah Pertama',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      code: 'SMA',
      name: 'Sekolah Menengah Atas',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      code: 'D3',
      name: 'Diploma',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      code: 'S1',
      name: 'Sarjana',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      code: 'S2',
      name: 'Master',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      code: 'S3',
      name: 'Doktor',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
