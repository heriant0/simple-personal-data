'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    await queryInterface.bulkInsert('master_religions', [{
      name: 'Islam',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Katolik',
      createdAt: new Date(),
      updatedAt: new Date()
    },{
      name: 'Protestan',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: 'Hindu',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: 'Budha',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
