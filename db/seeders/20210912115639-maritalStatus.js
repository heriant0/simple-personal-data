'use strict';

module.exports = {
  up: async (queryInterface, Sequelize) => {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */

    await queryInterface.bulkInsert('master_marital_statuses', [{
      name: 'Belum Menikah',
      createdAt: new Date(),
      updatedAt: new Date()
    },
    {
      name: 'Menikah',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: 'Duda',
      createdAt: new Date(),
      updatedAt: new Date()
    }, {
      name: 'Janda',
      createdAt: new Date(),
      updatedAt: new Date()
    }], {});
  },

  down: async (queryInterface, Sequelize) => {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
