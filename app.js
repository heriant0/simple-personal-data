if(process.env.NODE_ENV =="development"){
  require("dotenv").config()
}

const express = require('express')
const cors = require('cors')
const app = express()
const port = process.env.PORT || 3000
const routes = require('./routes')

app.use(cors())
app.use(express.json())
app.use(express.urlencoded({ extended: true }))

app.use('/', routes)

app.listen(port, () => {
  console.log(`This app listening on port ${port}`)
})

// const Sequelize = require('sequelize')
// const db = require('./config/database')
// const sequelize = new Sequelize(db.development)
// try {
//   sequelize.authenticate()
//   console.log('Connetion has been estabilished succesfully')
// } catch (e) {
//   console.error('Unable to connect to database', e)
// }