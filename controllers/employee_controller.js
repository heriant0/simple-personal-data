const response = require('../helpers/response')
const services = require('../services/employee_service')
const Sequelize = require('sequelize')
const db = require('../config/database')
const sequelize = new Sequelize(db.development)

class EmployeeController {

  static async findAll(req, res, next) {
    try {
      const result = await services.getAllData()
      return response.sucesss(res, next, 200, "Get data successfully", result)
    } catch (e) {
      console.log(e.message)
    }
  }

  static async findOne(req, res, next) {
    const id = req.params.id

    try {
      let result = await services.getOneData(id)
      return response.sucesss(res, next, 200, "Get data successfully", result)

    } catch (e) {
      console.log(e.message)
    }
  }

  static async add(req, res, next) {
    const data = req.body

    try {
      await sequelize.transaction(async (t) => {

        await services.saveData(data, t)
        return response.sucesss(res, next, 201, "Data Save Successfully", true)
      })
    } catch (e) {
      console.log(e.message)
    }
  }

  static async edit(req, res, next) {
    const id = req.params.id
    const data = req.body
    try {

      await sequelize.transaction(async (t) => {

        await services.updateData(id, data, t)
        return response.sucesss(res, next, 200, "Data updated Successfully", true)

      })
    } catch (e) {

    }
  }

  static async delete(req, res, next) {
    const id = req.params.id

    try {
      await sequelize.transaction(async (t) => {
        await services.deleteData(id, t)
        return response.sucesss(res, next, 200, "Data deleted Successfully", true)
      })
    } catch (e) {
      console.log(e.message)
    }
  }
}

module.exports = EmployeeController