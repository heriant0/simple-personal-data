const response = require('../helpers/response')
const services = require('../services/address_service')
const Sequelize = require('sequelize')
const db = require('../config/database')
const sequelize = new Sequelize(db.development)

class AddressController {

  static async findAllByEmployee(req, res, next) {
    const { employeeId } = req.query
    try {
      const result = await services.getAllDataEmployee(+employeeId)
      return response.sucesss(res, next, 200, "Get data successfully", result)
    } catch (e) {
      console.log(e.message)
    }
  }

  static async findOne(req, res, next) {
    const { addressTypeId, employeeId } = req.query

    try {
      let result = await services.getOneDataByEmployee(+addressTypeId, +employeeId)
      return response.sucesss(res, next, 200, "Get data successfully", result)

    } catch (e) {
      console.log(e.message)
    }
  }

  static async add(req, res, next) {
    const data = req.body

    try {
      await sequelize.transaction(async (t) => {

        await services.saveData(data, t)
        return response.sucesss(res, next, 201, "Data Saved Successfully", true)
      })
    } catch (e) {
      console.log(e)
    }
  }

  static async edit(req, res, next) {
    const id = req.params.id
    const data = req.body

    try {
      await sequelize.transaction(async (t) => {
        await services.updateData(id, data, t)
        return response.sucesss(res, next, 200, "Data updated Successfully", true)

      })
    } catch (e) {
      console.log(e)
    }
  }

  static async delete(req, res, next) {
    const id = req.params.id

    try {
      await sequelize.transaction(async (t) => {
        await services.deleteData(id, t)
        return response.sucesss(res, next, 200, "Data deleted Successfully", true)

      })
    } catch (e) {

    }
  }
}

module.exports = AddressController