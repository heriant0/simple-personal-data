const response = require('../helpers/response')
const services = require('../services/education_service')
const Sequelize = require('sequelize')
const db = require('../config/database')
const sequelize = new Sequelize(db.development)

class EducationController {

  static async findAll(req, res, next) {
    const employeeId = req.query.employeeId

    try {
      if (!employeeId) return response.sucesss(res, next, 200, "Invalid Employee ID", [])
      let result = await services.getAllData(+employeeId)
      return response.sucesss(res, next, 200, "Get data successfully", result)
    } catch (e) {
      console.log(e)
    }
  }

  static async findOne(req, res, next) {
    const id = req.params.id

    try {
      let result = await services.getOneData(id)
      return response.sucesss(res, next, 200, "Get data successfully", result)
    } catch (e) {
      console.log(e.message)
    }
  }

  static async add(req, res, next) {
    const data = req.body

    try {
      await sequelize.transaction(async (t) => {
        await services.saveData(data, t)
        return response.sucesss(res, next, 200, "Data saved successfully", true)
      })
    } catch (e) {
      console.log(e)
    }
  }

  static async edit(req, res, next) {
    const id = req.params.id
    const data = req.body

    try {
      await sequelize.transaction(async (t) => {
        await services.updateData(id, data, t)
        return response.sucesss(res, next, 200, "Data updated successfully", true)
      })
    } catch (e) {
      console.log(e)
    }
  }

  static async delete(req, res, next) {
    const id = req.params.id

    try {
      await sequelize.transaction(async (t) => {
        await services.deleteData(id, t)
        return response.sucesss(res, next, 200, "Data deleted successfully", true)
      })
    } catch (e) {
      console.log(e.message)
    }
  }
}

module.exports = EducationController