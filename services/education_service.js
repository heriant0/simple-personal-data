const { education, master_education_level } = require('../db/models')
const moment = require('moment-timezone')

const EducationServices = {
  getAllData: async (employeeId) => {

    try {
      let data = await education.findAll({
        where: { employee_id: employeeId },
        include: [{
          model: master_education_level,
          attributes: ['id', 'code', 'name']
        }]
      })

      let result = []
      if (data.length > 0) {
        data.forEach(el => {
          result.push({
            id: el.id,
            employeeId: el.employee_id,
            educationLevelId: el.education_level_id,
            startYear: el.start_year,
            graduationDate: el.graduation_date,
            major: el.major,
            gpa: el.gpa,
          })
        });
      }

      return result
    } catch (e) {
      throw new Error(e.message)
    }
  },

  getOneData: async (id) => {

    try {
      let data = await education.findOne({
        where: { id },
        include: [{
          model: master_education_level,
          attributes: ['id', 'code', 'name']
        }]
      })

      if (!data) throw new Error('Get data failed')

      let result = {
        id: data.id,
        employeeId: data.employee_id,
        educationLevelId: data.education_level_id,
        startYear: data.start_year,
        graduationDate: data.graduation_date,
        major: data.major,
        gpa: data.gpa,
      }

      return result
    } catch (e) {
      throw new Error(e.message)
    }
  },

  saveData: async (data, transaction) => {
    let employeeId = data.employeeId
    let educationLevelId = data.educationLevelId
    let startYear = data.startYear
    let graduationDate = moment(data.graduationDate, "DD-MM-YYYY").format("YYYY-MM-DD")
    let major = data.major
    let gpa = data.gpa

    try {
      let row = {
        employee_id: employeeId,
        education_level_id: educationLevelId,
        start_year: startYear,
        graduation_date: graduationDate,
        major: major,
        gpa: gpa,
      }

      const data = await education.create(row, { transaction })
      if (!data) throw new Error('Save data failed')
    } catch (e) {
      throw new Error(e.message)
    }
  },

  updateData: async (id, data, transaction) => {

    let employeeId = data.employeeId
    let educationLevelId = data.educationLevelId
    let startYear = data.startYear
    let graduationDate = moment(data.graduationDate, "DD-MM-YYYY").format("YYYY-MM-DD")
    let major = data.major
    let gpa = data.gpa

    try {

      let row = {
        employee_id: employeeId,
        education_level_id: educationLevelId,
        start_year: startYear,
        graduation_date: graduationDate,
        major: major,
        gpa: gpa,
      }

      let isUpdated = await education.update(row, { where: { id } }, { transaction })
      if (!isUpdated) throw new Error('Update data failed')
      return isUpdated
    } catch (e) {
      throw new Error(e)
    }
  },

  deleteData: async (id, transaction) => {
    try {
      let isDeleted = await education.destroy({ where: { id } }, { transaction })
      if (!isDeleted) throw new Error("Delete data failed")
      return isDeleted
    } catch (e) {
      throw new Error(e)
    }
  }
}

module.exports = EducationServices

