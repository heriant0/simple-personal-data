const { emergency_contact } = require('../db/models')


const EmergencycontactServices = {

  getAllData: async (employeeId) => {

    try {
      const data = await emergency_contact.findAll({ where: { employee_id: employeeId } })

      let result = []
      if (data.length > 0) {
        data.forEach(el => {
          result.push({
            id: el.id,
            employeeId: el.employee_id,
            name: el.name,
            relationship: el.relationship,
            phoneNumber: el.phone_number,
            address: el.address,
          })
        });

        return result
      }
    } catch (e) {
      throw new Error(e.message)
    }
  },

  getOneData: async (id) => {

    try {
      const data = await emergency_contact.findOne({ where: { id } })
      if (!data) throw new Error('Get data failed')
      let result = {
        id: data.id,
        employeeId: data.employee_id,
        name: data.name,
        relationship: data.relationship,
        phoneNumber: data.phone_number,
        address: data.address,
      }

      return result
    } catch (e) {
      throw new Error(e.message)

    }
  },

  saveData: async (data, transaction) => {
    let employeeId = data.employeeId
    let name = data.name
    let relationship = data.relationship
    let phoneNumber = data.phoneNumber
    let address = data.address

    try {
      let row = {
        employee_id: employeeId,
        name: name,
        relationship: relationship,
        phone_number: phoneNumber,
        address: address,
      }

      let data = await emergency_contact.create(row, { transaction })
      if (!data) throw new Error('Save data failed')

      return data
    } catch (e) {
      throw new Error(e.message)
    }
  },

  updateData: async (id, data, transaction) => {
    let employeeId = data.employeeId
    let name = data.name
    let relationship = data.relationship
    let phoneNumber = data.phoneNumber
    let address = data.address

    try {
      let row = {
        employee_id: employeeId,
        name: name,
        relationship: relationship,
        phone_number: phoneNumber,
        address: address,
      }

      let isUpdated = await emergency_contact.update(row, { where: { id } }, { transaction })
      if (!isUpdated) throw new Error('Save data failed')

      return isUpdated
    } catch (e) {
      throw new Error(e.message)

    }
  },

  deleteData: async (id, transaction) => {
    try {
      let isDeleted = await emergency_contact.destroy({ where: { id } }, { transaction })
      if (!isDeleted) throw new Error('Delete data failed')

      return isDeleted
    } catch (e) {
      throw new Error(e.message)

    }
  }

}

module.exports = EmergencycontactServices