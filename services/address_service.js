const { address } = require('../db/models')

const AddressServices = {

  getAllDataEmployee: async (employeeId) => {
    try {
      const data = await address.findAll({ where: { employee_id: employeeId } })

      let result = []
      if (data.length > 0) {
        data.forEach(el => {

          result.push({
            id: el.id,
            addressTypeId: el.address_type,
            addressType: (el.address_type == 1) ? "Home Address" : "Current Address",
            employeeId: el.employee_id,
            alamat: el.address,
            city: el.city,
            subDistrict: el.sub_district,
            district: el.district,
            postalCode: el.postal_code,
          })

        });
      }
      return result
    } catch (e) {
      throw new Error(e.message)
    }
  },

  getOneDataByEmployee: async (addressTypeId, employeeId) => {

    try {
      let data = await address.findOne({ where: { address_type: addressTypeId, employee_id: employeeId } })

      if (!data) throw new Error('Get data failed')

      let result = {
        id: el.id,
        addressTypeId: el.address_type,
        addressType: (el.address_type == 1) ? "Home Address" : "Current Address",
        employeeId: el.employee_id,
        alamat: el.address,
        city: el.city,
        subDistrict: el.sub_district,
        district: el.district,
        postalCode: el.postal_code,
      }
      return result
    } catch (e) {
      throw new Error(e.message)
    }
  },

  saveData: async (data, transaction) => {

    let addressTypeId = data.addressTypeId
    let employeeId = data.employeeId
    let alamat = data.address
    let city = data.city
    let subDistrict = data.subDistrict
    let district = data.district
    let postalCode = data.postalCode

    try {
      let row = {
        address_type: addressTypeId,
        employee_id: employeeId,
        address: alamat,
        city: city,
        sub_district: subDistrict,
        district: district,
        postal_code: postalCode,
      }

      let result = await address.create(row, { transaction })

      if (!result) throw new Error('Save data failed')
      return result
    } catch (e) {
      throw new Error(e.message)
    }
  },

  updateData: async (id, data) => {
    let addressTypeId = data.addressTypeId
    let employeeId = data.employeeId
    let alamat = data.address
    let city = data.city
    let subDistrict = data.subDistrict
    let district = data.district
    let postalCode = data.postalCode

    try {

      let row = {
        address_type: addressTypeId,
        employee_id: employeeId,
        address: alamat,
        city: city,
        sub_district: subDistrict,
        district: district,
        postal_code: postalCode,
      }

      let isUpdated = await address.update(row, { where: { id } })
      if (!isUpdated) throw new Error('Update data failed')

      return isUpdated

    } catch (e) {
      throw new Error(e.message)
    }
  },

  deleteData: async (id, transaction) => {

    try {

      const isDelete = await address.destroy({ where: { id } }, { transaction })
      if (!isDelete) throw new Error("Delete data failed")

      return isDelete
    } catch (e) {
      throw new Error(e.message)
    }
  }

}

module.exports = AddressServices
