const { employee, master_religion, master_marital_status } = require('../db/models')
const moment = require('moment-timezone')

const EmployeeServices = {

  getAllData: async () => {

    try {
      const data = await employee.findAll({
        include: [{
          model: master_religion,
          attributes: ['id', 'name']
        }, {
          model: master_marital_status,
          attributes: ['id', 'name']
        }]
      })

      let result = []
      if (data.length > 0) {
        data.forEach(el => {
          result.push({
            employeeId: el.id,
            employeeCode: el.employee_code,
            name: el.name,
            email: el.email,
            phoneNumber: el.phone_number,
            placeOfBirth: el.place_of_birth,
            dateOfBirth: moment(el.date_of_birth, "DD-MM-YYYY").format("YYYY-MM-DD"),
            gender: (el.gender == 1) ? "Laki-Laki" : (el.gender == 2) ? "Perempuan" : "Lainnya",
            religionId: el.religion_id,
            religionName: el.master_religion.name,
            bloodYype: el.blood_type,
            maritalStatusId: el.marital_status_id,
            maritalStatusName: el.master_marital_status.name,
            citizenship: el.citizenship,
            identityNumber: el.identity_number,
            npwpNumber: el.npwp_number,
          })
        });
      }
      return result
    } catch (e) {
      throw new Error(e.message)
    }
  },

  getOneData: async (id) => {
    try {
      const data = await employee.findOne({
        where: { id },
        include: [{
          model: master_religion,
          attributes: ['id', 'name']
        }, {
          model: master_marital_status,
          attributes: ['id', 'name']
        }]
      })

      if (!data) throw new Error('Get data failed')
      const result = {
        employeeId: data.id,
        employeeCode: data.employee_code,
        name: data.name,
        email: data.email,
        phoneNumber: data.phone_number,
        placeOfBirth: data.place_of_birth,
        dateOfBirth: moment(data.date_of_birth, "DD-MM-YYYY").format("YYYY-MM-DD"),
        gender: (data.gender == 1) ? "Laki-Laki" : (data.gender == 2) ? "Perempuan" : "Lainnya",
        religionId: data.religion_id,
        religionName: data.master_religion.name,
        bloodYype: data.blood_type,
        maritalStatusId: data.marital_status_id,
        maritalStatusName: data.master_marital_status.name,
        citizenship: data.citizenship,
        identityNumber: data.identity_number,
        npwpNumber: data.npwp_number,
      }
      return result
    } catch (e) {
      throw new Error(e.message)
    }
  },

  saveData: async (data, transaction) => {
    let employeeCode = data.employeeCode
    let name = data.name
    let email = data.email
    let phoneNumber = data.phoneNumber
    let placeOfBirth = data.placeOfBirth
    let dateOfBirth = data.dateOfBirth
    let gender = data.gender
    let religionId = data.religionId
    let bloodYype = data.bloodYype
    let maritalStatusId = data.maritalStatusId
    let citizenship = data.citizenship
    let identityNumber = data.identityNumber
    let npwpNumber = data.npwpNumber

    try {
      let row = {
        employee_code: employeeCode,
        name: name,
        email: email,
        phone_number: phoneNumber,
        place_of_birth: placeOfBirth,
        date_of_birth: moment(dateOfBirth, "DD-MM-YYYY").format("YYYY-MM-DD"),
        gender: gender,
        religion_id: religionId,
        blood_type: bloodYype,
        marital_status_id: maritalStatusId,
        citizenship: citizenship,
        identity_number: identityNumber,
        npwp_number: npwpNumber,
      }
      let result = await employee.create(row, { transaction })
      return result
    } catch (e) {
      throw new Error(e.message)
    }
  },

  updateData: async (id, data, transaction) => {
    let employeeCode = data.employeeCode
    let name = data.name
    let email = data.email
    let phoneNumber = data.phoneNumber
    let placeOfBirth = data.placeOfBirth
    let dateOfBirth = data.dateOfBirth
    let gender = data.gender
    let religionId = data.religionId
    let bloodYype = data.bloodYype
    let maritalStatusId = data.maritalStatusId
    let citizenship = data.citizenship
    let identityNumber = data.identityNumber
    let npwpNumber = data.npwpNumber

    try {
      let row = {
        employee_code: employeeCode,
        name: name,
        email: email,
        phone_number: phoneNumber,
        place_of_birth: placeOfBirth,
        date_of_birth: moment(dateOfBirth, "DD-MM-YYYY").format("YYYY-MM-DD"),
        gender: gender,
        religion_id: religionId,
        blood_type: bloodYype,
        marital_status_id: maritalStatusId,
        citizenship: citizenship,
        identity_number: identityNumber,
        npwp_number: npwpNumber,
      }

      let isUpdated = await employee.update(row, { where: { id } }, { transaction })
      if (!isUpdated) throw new Error('Update data failed')
      return isUpdated

    } catch (e) {
      throw new Error(e.message)
    }
  },

  deleteData: async (id, transaction) => {

    try {
      const isDeleted = await employee.destroy({ where: { id } }, { transaction })
      if (!isDeleted) throw new Error('Delete data failed')
    } catch (e) {
      throw new Error(e.message)
    }
  }

}

module.exports = EmployeeServices