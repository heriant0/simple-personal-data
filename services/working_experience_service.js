const { working_experience } = require('../db/models')
const moment = require('moment-timezone')

const WorkExperienceServices = {

  getAllData: async (employeeId) => {

    try {
      let data = await working_experience.findAll({ where: { employee_id: employeeId } })

      let result = []
      if (data.length > 0) {
        data.forEach(el => {
          result.push({
            id: el.id,
            employeeId: el.employee_id,
            companyName: el.company_name,
            businessIndustry: el.business_industry,
            lastPosition: el.last_position,
            jobDescription: el.job_description,
            joinDate: moment(el.join_date, "YYYY-MM-DD").format("DD-MM-YYYY"),
            resignationDate: moment(el.resignation_date, "YYYY-MM-DD").format("DD-MM-YYYY")
          })
        });
      }

      return result
    } catch (e) {
      throw new Error(e.message)
    }
  },

  getOneData: async (id) => {

    try {
      let data = await working_experience.findOne({ where: { id } })
      if (!data) throw new Error('Get data failed')

      let result = {
        employeeId: data.employee_id,
        companyName: data.company_name,
        businessIndustry: data.business_industry,
        lastPosition: data.last_position,
        jobDescription: data.job_description,
        joinDate: moment(data.join_date, "YYYY-MM-DD").format("DD-MM-YYYY"),
        resignationDate: moment(data.resignation_date, "YYYY-MM-DD").format("DD-MM-YYYY")
      }

      return result
    } catch (e) {
      throw new Error(e.message)
    }
  },

  saveData: async (data, transaction) => {
    let employeeId = data.employeeId
    let companyName = data.companyName
    let businessIndustry = data.businessIndustry
    let lastPosition = data.lastPosition
    let jobDescription = data.jobDescription
    let joinDate = data.joinDate
    let resignationDate = data.resignationDate

    try {
      let row = {
        employee_id: employeeId,
        company_name: companyName,
        business_industry: businessIndustry,
        last_position: lastPosition,
        job_description: jobDescription,
        join_date: moment(joinDate, "DD-MM-YYYY").format("YYYY-MM-DD"),
        resignation_date: moment(resignationDate, "DD-MM-YYYY").format("YYYY-MM-DD")
      }

      let dataSaved = await working_experience.create(row, { transaction })
      if (!dataSaved) throw new Error("Save data failed")

      return dataSaved
    } catch (e) {
      throw new Error(e.message);
    }
  },

  updateData: async (id, data, transaction) => {

    let employeeId = data.employeeId
    let companyName = data.companyName
    let businessIndustry = data.businessIndustry
    let lastPosition = data.lastPosition
    let jobDescription = data.jobDescription
    let joinDate = data.joinDate
    let resignationDate = data.resignationDate

    try {
      let row = {
        employee_id: employeeId,
        company_name: companyName,
        business_industry: businessIndustry,
        last_position: lastPosition,
        job_description: jobDescription,
        join_date: moment(joinDate, "DD-MM-YYYY").format("YYYY-MM-DD"),
        resignation_date: moment(resignationDate, "DD-MM-YYYY").format("YYYY-MM-DD")
      }

      let isUpdated = await working_experience.update(row, { where: { id } }, { transaction })
      if (!isUpdated) throw new Error('Update data failed')
      return isUpdated
    } catch (e) {
      throw new Error(e.message);
    }
  },

  deleteData: async (id, transaction) => {

    try {
      let isDeleted = await working_experience.destroy({ where: { id } }, { transaction })
      if (!isDeleted) throw new Error('Update data failed')

      return isDeleted
    } catch (e) {
      throw new Error(e.message);

    }
  }


}

module.exports = WorkExperienceServices