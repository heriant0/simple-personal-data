'use-strict'
const router = require('express').Router();
const employeeRoutes = require('./employee_route')
const addressRoutes = require('./address_route')
const educationRoutes = require('./education_route')
const emergencyContactRoutes = require('./emergency_contact_route')
const workingExperienceRoutes = require('./working_experience_route')


router.use('/employees', employeeRoutes)
router.use('/addresses', addressRoutes)
router.use('/educations', educationRoutes)
router.use('/emergencycontacts', emergencyContactRoutes)
router.use('/workingexperiences', workingExperienceRoutes)

module.exports = router