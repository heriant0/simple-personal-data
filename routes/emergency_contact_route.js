const router = require('express').Router()
const emergencyContactController = require('../controllers/emergency_contact_controller')

router.get('/', emergencyContactController.findAll)
router.get('/:id', emergencyContactController.findOne)
router.post('/', emergencyContactController.add)
router.put('/:id', emergencyContactController.edit)
router.delete('/:id', emergencyContactController.delete)

module.exports = router