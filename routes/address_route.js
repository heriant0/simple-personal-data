const router = require('express').Router()
const addressController = require('../controllers/address_controller')

router.get('/', addressController.findAllByEmployee)
router.get('/bytype', addressController.findOne)
router.post('/', addressController.add)
router.put('/:id', addressController.edit)
router.delete('/:id', addressController.delete)

module.exports = router