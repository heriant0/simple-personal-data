const router = require('express').Router()
const wokringExperienceController = require('../controllers/working_experience_controller')

router.get('/', wokringExperienceController.findAll)
router.get('/:id', wokringExperienceController.findOne)
router.post('/', wokringExperienceController.add)
router.put('/:id', wokringExperienceController.edit)
router.delete('/:id', wokringExperienceController.delete)

module.exports = router