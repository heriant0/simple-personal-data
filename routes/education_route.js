const router = require('express').Router()
const educationController = require('../controllers/education_controller')

router.get('/', educationController.findAll)
router.get('/:id', educationController.findOne)
router.post('/', educationController.add)
router.put('/:id', educationController.edit)
router.delete('/:id', educationController.delete)

module.exports = router