const router = require('express').Router()
const employeeCOntroller = require('../controllers/employee_controller')

router.get('/', employeeCOntroller.findAll)
router.get('/:id', employeeCOntroller.findOne)
router.post('/', employeeCOntroller.add)
router.put('/:id', employeeCOntroller.edit)
router.delete('/:id', employeeCOntroller.delete)

module.exports = router