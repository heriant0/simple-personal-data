# simple-personal-data

# Langkah Menjalankan Aplikasi

- nmp i
- install sequelize-cli
- jalankan command berikut:
  - sequelize db:migrate
  - sequelize db:seed:all

- Aplikasi ini hanya dari sisi server saja 
- Aplikasi ini dapat menyimpan data personal karyawan seperti :
  - Data Personal
  - Data Alamat
  - Data Pengalaman Kerja
  - Data Pendidikan dan
  - Data Kontak Darurat

- Kekuragan dalam sistem:
  - Belum ditambahkan error handling
- Fitur yang mungkin bisa ditambhkan ketika sudah menjadi karyawan:
  - Historycal Jabatan
  - Historical Departement
  - Historical Status Kerja
  
Berikut Endpoints yang dapat diakses:
- Endpoints Personal Data
  - `POST /employees`
  - `GET /employees`
  - `GET /employees/:id`
  - `PUT /employees/id`
  - `DELETE /employees/:id`

- Endpoints Alamat
  - `POST /addresses`
  - `GET /addresses`
  - `GET /addresses/:id`
  - `PUT /addresses/id`
  - `DELETE /addresses/:id`

- Endpoints Alamat
  - `POST /addresses`
  - `GET /addresses`
  - `GET /addresses/:id`
  - `PUT /addresses/id`
  - `DELETE /addresses/:id`

- Endpoints Pendidikan
  - `POST /educations`
  - `GET /educations`
  - `GET /educations/:id`
  - `PUT /educations/id`
  - `DELETE /educations/:id`

- Endpoints Pengalaman Kerja
  - `POST /workingexperiences`
  - `GET /workingexperiences`
  - `GET /workingexperiences/:id`
  - `PUT /workingexperiences/id`
  - `DELETE /workingexperiences/:id`

- Endpoints Kontak Darurat
  - `POST /emergencycontacts`
  - `GET /emergencycontacts`
  - `GET /emergencycontacts/:id`
  - `PUT /emergencycontacts/id`
  - `DELETE /emergencycontacts/:id`

Example: 

### POST /employees

Request:

- data:

```json
{
    "employeeCode": "EMP-001",
    "name": "Herianto",
    "email": "heri@mail.com",
    "phoneNumber": "0812427765",
    "placeOfBirth": "Makassar",
    "dateOfBirth": "29-08-1990",
    "gender": 1,
    "religionId": 1,
    "bloodYype": "O",
    "maritalStatusId": 2,
    "citizenship": "Indonesia",
    "identityNumber": "123456789",
    "npwpNumber": "2345678"
}
```

Response:

- status: 201
- body:  ​

```json
{
  "code": "integer",
  "message": "string",
  "data": "boolean",
}
```

### POST /addresses

Request:

- data:

```json
{
    "addressTypeId": 1,
    "employeeId": 1,
    "address": "Jl. Prapatan 1",
    "city": "Jakarta",
    "subDistrict": "Senen",
    "district": "Senen",
    "postalCode": "10410"
}
```
Response:

- status: 201
- body:  ​

```json
{
  "code": "integer",
  "message": "string",
  "boolean"
}

```
### POST /educatons

Request:

- data:

```json
{
   {
    "employeeId": 1,
    "educationLevelId": 5 ,
    "startYear": 2009,
    "graduationDate": "20-04-2014" ,
    "major": "Teknink Informatika",
    "gpa": 3.75
}
}
```
Response:

- status: 201
- body:  ​

```json
{
  "code": "integer",
  "message": "string",
  "data": "boolean"
}
```

### POST /workingexperiences

Request:

- data:

```json
{
   {
    "employeeId": 1,
    "companyName": "PT. XXX",
    "businessIndustry": "Teknologi Informasi",
    "lastPosition": "Software Engineer",
    "jobDescription": "coding",
    "joinDate": "27-06-2013",
    "resignationDate": "23-11-2019"
  }
}
```
Response:

- status: 201
- body:  ​

```json
{
  "code": "integer",
  "message": "string",
  "data": "boolean"
}
```

### POST /workingexperiences

Request:

- data:

```json
{
  {
    "employeeId": 1,
    "name": "Ucok",
    "relationship": "Ayah" ,
    "phoneNumber": "081222xxxx",
    "address": "Jakarta"
  }
}
```
Response:

- status: 201
- body:  ​

```json
{
  "code": "integer",
  "message": "string",
  "data": "boolean"
}
```
